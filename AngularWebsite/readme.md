**In IIS**

Create one website having the correct routing to the root folder of the site. This is the place where the web.config
from the WebsiteRewriter is located. This web.config will rewrite incoming URLs as required. 

Create applications within the main IIS Website for the new Angular application and the old existing website and point
the location of these to the correct folders. These application have Virtual Path names. Use these names as the rewrite
names in you web.config. New V2 and Old V1 is used as an example.

Result should look something like this:
https://gyazo.com/6215abe0d3d9335120050ea1fcec4b5d

